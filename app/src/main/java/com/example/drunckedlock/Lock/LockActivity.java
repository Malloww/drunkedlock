package com.example.drunckedlock.Lock;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.drunckedlock.Answer.CorrectAnswerActivity;
import com.example.drunckedlock.Answer.WrongAnswerActivity;
import com.example.drunckedlock.Map.MapActivity;
import com.example.drunckedlock.R;
import com.example.drunckedlock.Tools.Answer;
import com.example.drunckedlock.Tools.MySQLiteHelper;
import com.example.drunckedlock.Tools.Quest;

import java.util.ArrayList;
import java.util.Random;

public class LockActivity extends Activity{

    private boolean answer;

    private ArrayList<Quest> listeQuestions;
    private ArrayList<Answer> listeReponses;

    private Quest question;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lock);

        Toast.makeText(this.getApplicationContext(), "Merci de définir DrunckedLock comme application par défaut et de cliquer sur \"Toujours\", vous pourrez changer ce choix une fois votre beuverie terminée.", Toast.LENGTH_LONG).show();
        startActivityForResult(new Intent(Settings.ACTION_HOME_SETTINGS),0);

        //Code du bouton pour dévérouiller le téléphone (au cas où...)
        /*findViewById(R.id.UnlockButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Settings.ACTION_HOME_SETTINGS),0);
                Toast.makeText(v.getContext(), "Vous pouvez rétablir votre application de menu par defaut ou laisser DrunckedLock si vous souhaitez continuer de boire.", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(LockActivity.this, MainActivity.class));
                finish();
            }
        });
        */

        findViewById(R.id.fabMap).setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        startActivity(new Intent(LockActivity.this, MapActivity.class )); }});

        // Quand on répond correctement à la question
        findViewById(R.id.fabAnswer).setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        if (answer == true)
                            startActivity(new Intent(LockActivity.this, CorrectAnswerActivity.class));
                        else
                            startActivity(new Intent(LockActivity.this, WrongAnswerActivity.class));
                        finish();
                    }
                });

        displayQuestion();
        sendMessage();
    }

    @Override
    public void onBackPressed(){
        //Do nothing
    }

    public void resetPreferredLauncherAndOpenChooser() {

    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioAnswer1: //Si le premier bouton radio est coché
                if (checked)
                    answer = this.listeReponses.get(0).getGOOD();
                break;
            case R.id.radioAnswer2:
                if (checked)
                    answer = this.listeReponses.get(1).getGOOD();
                break;
            case R.id.radioAnswer3:
                if (checked)
                    answer = this.listeReponses.get(2).getGOOD();
                break;
            case R.id.radioAnswer4: // Mauvaise réponse
                if (checked)
                    answer = this.listeReponses.get(3).getGOOD();
                break;
        }
    }

    public void displayQuestion(){
        //On récupère toutes les questions
        this.listeQuestions = Quest.selectAll((new MySQLiteHelper(this.getApplicationContext())).getWritableDatabase());

        //On récupère une question aléatoire
        this.question = this.listeQuestions.get(new Random().nextInt(this.listeQuestions.size()));

        //On prend les réponses
        this.listeReponses = question.getReponseList();

        //On affiche la question dans le TextView
        ((TextView) findViewById(R.id.textQuestion)).setText(this.question.getLBL_QUE());

        //On affiche les réponses dans les boutons radio
        ((RadioButton) findViewById(R.id.radioAnswer1)).setText(this.listeReponses.get(0).getLBL_REP());
        ((RadioButton) findViewById(R.id.radioAnswer2)).setText(this.listeReponses.get(1).getLBL_REP());
        ((RadioButton) findViewById(R.id.radioAnswer3)).setText(this.listeReponses.get(2).getLBL_REP());
        ((RadioButton) findViewById(R.id.radioAnswer4)).setText(this.listeReponses.get(3).getLBL_REP());
    }


    /* -------- PARTIE ENVOIE DU SMS -------- */

    public void sendMessage() {
        String mPhoneNumber = getSharedPreferences("com.example.drunckedlock", MODE_PRIVATE).getString("phone", "0000");

        // Pour l'envoie de SMS
        final Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", mPhoneNumber);
        smsIntent.putExtra("sms_body","Au secours, j'ai besoin d'aide ! Viens me chercher s'il-te-plait.\n\n" +
                "Mon application DrunkedLock a détecté que je suis en état d'ébriété trop avancé pour pouvoir rentrer et envoyer un message tout seul...");

        findViewById(R.id.fabMessage).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(smsIntent));
            }
        });
    }
}