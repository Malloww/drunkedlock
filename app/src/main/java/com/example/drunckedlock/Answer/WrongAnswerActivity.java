package com.example.drunckedlock.Answer;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;

import com.example.drunckedlock.Lock.LockActivity;
import com.example.drunckedlock.Map.MapActivity;
import com.example.drunckedlock.R;

public class WrongAnswerActivity extends Activity {
    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wronganswer);

        findViewById(R.id.fabMap).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(WrongAnswerActivity.this, MapActivity.class));
            }
        });

        // Quand on souhaite réessayer
        findViewById(R.id.buttonRetry).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startActivity(new Intent(WrongAnswerActivity.this, LockActivity.class));
                finish();
            }
        });
        sendMessage();
    }

    /* -------- PARTIE ENVOIE DU SMS -------- */

    private String mPhoneNumber;

    public void sendMessage() {
        SharedPreferences sharedPref = getSharedPreferences("com.example.drunckedlock", MODE_PRIVATE);
        mPhoneNumber = sharedPref.getString("phone", "0000");

        // Pour l'envoie de SMS
        final Intent smsIntent = new Intent(Intent.ACTION_VIEW);
        smsIntent.setType("vnd.android-dir/mms-sms");
        smsIntent.putExtra("address", mPhoneNumber);
        smsIntent.putExtra("sms_body","Au secours, j'ai besoin d'aide ! Viens me chercher s'il-te-plait.\n\n" +
                "Mon application DrunkedLock a détecté que je suis en état d'ébriété trop avancé pour pouvoir rentrer et envoyer un message tout seul...");

        findViewById(R.id.fabMessage).setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        startActivity(new Intent(smsIntent));}});
    }
}
