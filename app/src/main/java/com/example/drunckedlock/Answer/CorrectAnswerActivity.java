package com.example.drunckedlock.Answer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.drunckedlock.R;

public class CorrectAnswerActivity extends Activity {
    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_correctanswer);

        // Pour "fermer" l'application, c'est-à-dire pour simuler un input sur le bouton home
        final Intent homeIntent = new Intent(Intent.ACTION_MAIN);
        homeIntent.addCategory( Intent.CATEGORY_HOME );
        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        findViewById(R.id.buttonCloseApp).setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        startActivity(new Intent(homeIntent));
                        finish();}});
    }
}
