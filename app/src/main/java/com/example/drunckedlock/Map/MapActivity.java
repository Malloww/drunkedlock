package com.example.drunckedlock.Map;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.example.drunckedlock.R;

public class MapActivity extends FragmentActivity {

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //load/initialize the osmdroid configuration, this can be done
        Context ctx = getApplicationContext();
        //Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        //Call the map fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.map_fragment,new MapFragment().newInstance(ctx))
                .commit();

        //Go to the xml of map
        setContentView(R.layout.map);
    }

    public void onResume() {
        super.onResume();
        //mapView.onResume();
    }

    public void onPause() {
        super.onPause();
        //mapView.onPause();
    }

    public void drawItinerary() {
    }
}