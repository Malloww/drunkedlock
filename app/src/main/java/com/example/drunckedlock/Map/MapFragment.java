package com.example.drunckedlock.Map;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.drunckedlock.R;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;
import org.osmdroid.views.overlay.gestures.RotationGestureOverlay;

public class MapFragment extends Fragment
{
    public static MapView mapView = null;
    private LocationManager locationManager;
    private static String PROVIDER = LocationManager.GPS_PROVIDER;
    private boolean locationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final String TAG = "MapsActivityProblem";
    private Marker geoLocationMarker;
    private GeoPoint geoLocationPoint = new GeoPoint(0,0);
    private boolean followMe = true;
    protected Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.map_fragment, container, false);
    }

    //When the fragment is instancing create and declare all object of the fragment
    public static MapFragment newInstance(Context ctx)
    {
        MapFragment fragment = new MapFragment();
        fragment.setmContext(ctx);
        return fragment;
    }

    public void setmContext(Context ctx)
    {
        this.mContext = ctx;
    }

    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //Create a localisation with a latitude and a longitude given
            geoLocationPoint = new GeoPoint(location.getLatitude(), location.getLongitude());
            geoLocationMarker.setIcon(getResources().getDrawable(R.drawable.icongeolocation));
            geoLocationMarker.setTitle("Your position");
            //Change the marker of the geolocation to the new position
            geoLocationMarker.setPosition(geoLocationPoint);
            mapView.getOverlays().add(geoLocationMarker);
            if (followMe) {
                IMapController mapController = mapView.getController();
                mapController.setCenter(geoLocationPoint);
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
            getLocationPermission();
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(mContext, "Veuillez activer votre géolocalisation", Toast.LENGTH_SHORT).show();
            geoLocationMarker.setVisible(false);
        }
    };

    //Ask the to grant the localisation
    protected void getLocationPermission() {
        //If the user granted yet the access to his localisation, if not ask him (Go to on onRequestPermissionsResult)
        if (ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
            getLocation();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    //Function to determine the answer of user about the permission to access to his location
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Permission was granted
                    locationPermissionGranted = true;
                    Toast.makeText(mContext, "Permission granted", Toast.LENGTH_SHORT).show();
                    getLocation();

                } else {

                    //Permission was disabled
                    locationPermissionGranted = false;
                    Toast.makeText(mContext, "Permission denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        /****Instance all Object*****/

        /**MapView Object**/
        mapView = view.findViewById(R.id.map);

        //load/initialize the osmdroid configuration, this can be done
        Configuration.getInstance().load(mContext, PreferenceManager.getDefaultSharedPreferences(mContext));
        //Create the map
        mapView.setTileSource(TileSourceFactory.MAPNIK);
        mapView.setBuiltInZoomControls(true);
        //Enable zoom with fingers
        mapView.setMultiTouchControls(true);
        IMapController mapController = mapView.getController();
        //Initialize zoom of map
        mapController.setZoom(9.5);

        //Add compass
        CompassOverlay compass = new CompassOverlay(mContext, new InternalCompassOrientationProvider(mContext), mapView);
        compass.enableCompass();
        mapView.getOverlays().add(compass);

        //Add map rotation
        RotationGestureOverlay rotationGesture = new RotationGestureOverlay(mContext, mapView);
        rotationGesture.setEnabled(true);
        mapView.setMultiTouchControls(true);
        mapView.getOverlays().add(rotationGesture);

        //Create a localisation with a latitude and a longitude given
        GeoPoint startPoint = new GeoPoint(48.8583, 2.2944);

        //Add a marker to the position given
        Marker startMarker = new Marker(mapView);
        startMarker.setPosition(startPoint);
        mapView.getOverlays().add(startMarker);
        //Changer the icon of the marker
        startMarker.setIcon(getResources().getDrawable(R.drawable.iconvertmap));
        //Add a title in the bubble
        startMarker.setTitle("Start point");

        //Center map marker's position
        mapController.setCenter(startPoint);

        //Initialisation of the geolocation's marker
        geoLocationMarker = new Marker(mapView);
        geoLocationMarker.setIcon(getResources().getDrawable(R.drawable.icongeolocation));
        geoLocationMarker.setTitle("Your position");

        getLocationPermission();

        //When you click on the button, you center the screen to the geolocation's marker
        Button buttonCenterMe = view.findViewById(R.id.buttonCenterMe);
        buttonCenterMe.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                buttonCenterMe(buttonCenterMe);
            }
        });

        Button buttonItinerary = view.findViewById(R.id.itinerary);
        buttonItinerary.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                ItineraryFragment itineraryFragment= new ItineraryFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.itinerary_fragment, itineraryFragment)
                        .addToBackStack(null)
                        .commit();

            }
        });
    }

    private void getLocation()
    {
        try {
            //Check of the user granted the permission of locate him
            if (locationPermissionGranted)
            {
                locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

                if(locationManager.isProviderEnabled(PROVIDER))
                {
                    geoLocationMarker.setIcon(getResources().getDrawable(R.drawable.icongeolocationgrey));
                    geoLocationMarker.setTitle("Your last know position");
                    geoLocationMarker.setVisible(true);
                    //Call onLocationChanged to get the current location of the user
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,3000, 10, locationListener);
                }
                else
                {
                    Toast.makeText(mContext, "Veuillez activer votre géolocalisation", Toast.LENGTH_LONG).show();
                    geoLocationMarker.setVisible(false);
                }
            }
        } catch (SecurityException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    //Function to center the screen to the geolocation's marker and decide to follow him or not
    private void buttonCenterMe(Button buttonCenterMe)
    {
        IMapController mapController = mapView.getController();
        mapController.setCenter(geoLocationPoint);
        mapController.setZoom(17);

        if(!followMe)
        {
            //Change the background button (Add the circle)
            buttonCenterMe.setBackgroundResource(R.drawable.ic_menu_mylocation);
            followMe = true;

        }
        else {

            //Change the background button (Remove the circle)
            buttonCenterMe.setBackgroundResource(R.drawable.ic_menu_mylocation_nf);
            followMe = false;
        }
    }
}