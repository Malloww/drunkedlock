package com.example.drunckedlock.Map;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.drunckedlock.Openrouteservice.ItineraryActivity;
import com.example.drunckedlock.R;

public class ItineraryFragment extends Fragment
{
    //When the fragment is instancing, do this
    public static ItineraryFragment newInstance(String title)
    {
        ItineraryFragment fragment = new ItineraryFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.itinerary_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        Button getItinerary = view.findViewById(R.id.getItinerary);
        getItinerary.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                new ItineraryActivity().execute();
            }
        });
    }
}
