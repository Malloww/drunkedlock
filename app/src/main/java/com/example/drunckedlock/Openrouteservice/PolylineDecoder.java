package com.example.drunckedlock.Openrouteservice;

import org.osmdroid.util.GeoPoint;

import java.util.ArrayList;
import java.util.List;

/**
 * Function for decode a Polyline
 *
 * Author : https://github.com/scoutant
 * Licence GPL v3
 */

public class PolylineDecoder
{
    private static final double DEFAULT_PRECISION = 1E5;

    public List<GeoPoint> decode(String encoded)
    {
        return decode(encoded, DEFAULT_PRECISION);
    }

    /**
     * Precision should be something like 1E5 or 1E6. For OSRM routes found precision was 1E6, not the original default
     * 1E5.
     *
     * @param encoded
     * @param precision
     * @return
     */
    public List<GeoPoint> decode(String encoded, double precision) {
        List<GeoPoint> track = new ArrayList<GeoPoint>();
        int index = 0;
        int lat = 0, lng = 0;

        while (index < encoded.length()) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            GeoPoint p = new GeoPoint((double) lat / precision,(double) lng / precision);
            track.add(p);
        }
        return track;
    }
}