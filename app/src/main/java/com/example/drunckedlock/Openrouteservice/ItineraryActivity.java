package com.example.drunckedlock.Openrouteservice;

import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import com.example.drunckedlock.Map.MapActivity;
import com.example.drunckedlock.Map.MapFragment;

import java.io.IOException;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.overlay.Polyline;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ItineraryActivity extends AsyncTask<Void, Void ,Response>
{

    @Override
    protected Response doInBackground(Void... voids)
    {

        OkHttpClient client = new OkHttpClient();
        String url = "https://api.openrouteservice.org/directions?api_key=5b3ce3597851110001cf62483d40a0e64ca34f25aee483705d979fac&coordinates=6.128005,45.902204|6.154844,45.919399&profile=foot-walking";

        //Send the request to Openrouteservice
        Request request = new Request.Builder()
                .url(url)
                .build();

        //Get the response
        Response response = null;
        try
        {
            response = client.newCall(request).execute();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        //return the response to onPostExecute
        return response;
    }

    @Override
    protected void onPostExecute(Response response)
    {
        if (response == null)
        {
            Log.i("tag", "----------------------> La requete à rendu null !!");
        }
        else {

            String polylineEncoded = null;
            PolylineDecoder decoder = new PolylineDecoder();
            Polyline polyline = new Polyline();

            try
            {
                //Get the polyline encoded returned by openRouteService
                JSONObject responseInJsonObject = new JSONObject(response.body().string());
                JSONArray responseToRoute = responseInJsonObject.getJSONArray("routes");
                JSONObject routeToJsonObject = responseToRoute.getJSONObject(0);
                polylineEncoded = routeToJsonObject.getString("geometry");
                Log.i("tag2", "----------------------> Réponse to string : " + polylineEncoded);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            //Decode the polyline in geopoint and add it in a polyline object
            List<GeoPoint> listGeoPoint = decoder.decode(polylineEncoded);
            polyline.setPoints(listGeoPoint);

            //Change the color of the polyline and draw the polyline
            polyline.setColor(Color.rgb(100,100,200));
            MapFragment.mapView.getOverlayManager().add(polyline);
            IMapController mapController = MapFragment.mapView.getController();
            mapController.setCenter(listGeoPoint.get(listGeoPoint.size()/2));
            mapController.setZoom(15);
        }
    }
}
