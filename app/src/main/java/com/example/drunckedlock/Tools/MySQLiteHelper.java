package com.example.drunckedlock.Tools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

//SGB class
public class MySQLiteHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "DrunckBDD";
    private static final Integer DATABASE_VERSION = 1;
    private static final String DATABASE_CREATE1 = "CREATE TABLE IF NOT EXISTS QUESTION (ID_QUE integer primary key NOT NULL , LBL_QUE text NOT NULL, PERSO tinyint(1) NOT NULL DEFAULT 0)";
    private static final String DATABASE_CREATE2 = "CREATE TABLE IF NOT EXISTS REPONSE  (ID_REP integer primary key NOT NULL , LBL_REP text NOT NULL, GOOD  tinyint(1) NOT NULL DEFAULT 0, ID_QUE integer NOT NULL, FOREIGN KEY(ID_QUE) REFERENCES QUESTION(ID_QUE) )";
    private static final String DATABASE_CREATE3 = "CREATE VIEW SELECT_ALL  AS  select Q.ID_QUE AS ID_QUE,Q.LBL_QUE AS LBL_QUE, R.LBL_REP AS LBL_REP, R.GOOD AS GOOD from QUESTION Q INNER JOIN REPONSE R ON R.ID_QUE = Q.ID_QUE order by Q.ID_QUE";

    public static void dropOld(SQLiteDatabase db) {
        db.execSQL("DELETE FROM REPONSE ");
        db.execSQL("DELETE FROM QUESTION");
    }

    public SQLiteDatabase getDb() {
        return this.getWritableDatabase();
    }

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("INFO", "----------------->BDD Create");
        db.execSQL(DATABASE_CREATE1);
        db.execSQL(DATABASE_CREATE2);
        db.execSQL(DATABASE_CREATE3);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS REPONSE");
        db.execSQL("DROP TABLE IF EXISTS QUESTION");
        db.execSQL("DROP VIEW  IF EXISTS SELECT_ALL");
        onCreate(db);
    }
}
