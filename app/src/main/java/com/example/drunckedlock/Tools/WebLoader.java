package com.example.drunckedlock.Tools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;
import android.support.v4.content.AsyncTaskLoader;

import org.json.JSONException;

import java.util.ArrayList;

public class WebLoader extends AsyncTaskLoader<ArrayList<Quest>> {
    private String sg;

    public WebLoader(Context c) {
        super(c);
    }

    public String getSg() {
        return sg;
    }

    public void setSg(String sg) {
        this.sg = sg;
    }

    @Nullable
    @Override
    public ArrayList<Quest> loadInBackground() {
        try {
            return Quest.parseJson(this.getSg());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    public void SyncBDD() { //sync with API
        ArrayList<Quest> ListQuest = this.loadInBackground(); //Load in async task
        SQLiteDatabase db = (new MySQLiteHelper(this.getContext())).getWritableDatabase();
        MySQLiteHelper.dropOld(db); // Delete old Quests and Answers
        for (int i = 0; i < ListQuest.size(); i++) {
            ListQuest.get(i).insert(db, true); //Insert all new Quest in local bdd
        }
    }
}
