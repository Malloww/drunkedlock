package com.example.drunckedlock.Tools;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class NetworkTools extends AsyncTask<Void, Void, Response> {
    private Context c;

    public Context getC() {
        return c;
    }

    public void setC(Context c) {
        this.c = c;
    }

    @Override//make the async task
    protected Response doInBackground(Void... voids) {

        OkHttpClient client = new OkHttpClient();
        String url = "https://drunkedlock.000webhostapp.com/?S=true";//url of APIe

        Response response = null;
        try {
            response = client.newCall(new Request.Builder().url(url).build()).execute();//build request
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }


    @Override//execute after answer of okhttp
    protected void onPostExecute(Response response) {
        if (response == null) {
            Log.i("RETURN", "----------------------> La requete à rendu null !!");
        } else {
            WebLoader WL = new WebLoader(this.getC());
            String rep = null;
            try {
                rep = response.body().string();//get the text of body
                WL.setSg(rep);
            } catch (Exception e) {
                Log.i("ERROR L51", "-------------------->" + e.getMessage());
            }
            WL.SyncBDD();
        }
    }
}
