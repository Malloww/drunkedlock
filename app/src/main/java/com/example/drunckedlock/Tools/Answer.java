package com.example.drunckedlock.Tools;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.Html;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Answer {
    private Integer ID_REP;
    private Integer ID_QUE;
    private String LBL_REP;
    private Boolean GOOD;

    public Answer(Integer ID_REP, Integer ID_QUE, String LBL_REP, Boolean GOOD) {
        this.setID_REP(ID_REP);//ID
        this.setID_QUE(ID_QUE);//ForeignKey
        this.setLBL_REP(LBL_REP);//String of answer
        this.setGOOD(GOOD);//true if this is the correct answers
    }

    //Convert response to Answer object
    public static Answer parseJson(String rep) throws JSONException {

        JSONObject results = new JSONObject(rep);

        try {
            //Get each json key for put to in Answer object
            return new Answer(results.getInt("ID_REP"), results.getInt("ID_QUE"), results.getString("LBL_REP"), (results.getInt("GOOD")) == 1);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("ERROR L37", "-------------->" + e.getMessage());
        }
        return null;
    }

    //search all aswers where string answer like this
    public static ArrayList<Answer> selectManyLikeQuest(SQLiteDatabase db, Integer ID_QUE) {
        Cursor c = db.rawQuery("SELECT * FROM REPONSE WHERE ID_QUE =" + ID_QUE, null);//the bdd request
        c.moveToFirst();
        ArrayList<Answer> LQ = new ArrayList<Answer>();

        for (int i = 0; i < c.getCount(); i++) {//convert all result to object
            Answer temp = new Answer(c.getInt(0), c.getInt(3), c.getString(1), c.getInt(2) == 1);
            LQ.add(temp);
            c.move(1);
        }
        c.close();
        return LQ;
    }

    //select all answer in local database
    public static ArrayList<Answer> selectAll(SQLiteDatabase db) {
        Cursor c = db.rawQuery("SELECT * FROM REPONSE", null);
        c.moveToFirst();
        ArrayList<Answer> LQ = null;

        for (int i = 0; i < c.getCount(); i++) {//convert all result to object
            LQ.add(new Answer(c.getInt(0), c.getInt(3), c.getString(1), c.getInt(2) == 1));
        }
        c.close();
        return LQ;
    }

    //insert one answer and his answer on local bdd
    public void insert(SQLiteDatabase db) {
        db.execSQL("INSERT INTO REPONSE (ID_QUE, LBL_REP, GOOD) VALUES (" + this.getID_QUE() + ",\"" + this.LBL_REP + "\"," + ((this.getGOOD()) ? 1 : 0) + ")");
    }

    //select one quest on local bdd
    public Answer selectOne(SQLiteDatabase db) {
        Cursor c = db.rawQuery("SELECT * FROM REPONSE WHERE ID_REP = " + this.getID_REP(), null);
        c.moveToFirst();

        return new Answer(c.getInt(0), c.getInt(3), c.getString(1), (c.getInt(2) == 1));
    }

    //search all answer where string of answer like this
    public ArrayList<Answer> selectManyLikeThis(SQLiteDatabase db) {
        Cursor c = db.rawQuery("SELECT * FROM REPONSE WHERE LBL_REP LIKE %" + this.getLBL_REP() + "%", null);
        c.moveToFirst();
        ArrayList<Answer> LQ = null;

        for (int i = 0; i < c.getCount(); i++) {
            LQ.add(new Answer(c.getInt(0), c.getInt(3), c.getString(1), c.getInt(2) == 1));
        }
        c.close();
        return LQ;
    }

    //update one quest in local bdd
    public void update(SQLiteDatabase db) {
        db.execSQL("UPDATE REPONSE SET ID_QUE = " + this.getID_QUE() + ", LBL_REP = \"" + this.getLBL_REP() + "\",GOOD = " + ((this.getGOOD()) ? 1 : 0) + " WHERE ID_REP = " + this.getID_REP());
    }

    //delete this quest in local bdd
    public void delete(SQLiteDatabase db) {
        db.execSQL("DELETE FROM REPONSE WHERE ID_REP = " + this.getID_REP());
    }

    public Integer getID_QUE() {
        return ID_QUE;
    }

    public void setID_QUE(Integer ID_QUE) {
        this.ID_QUE = ID_QUE;
    }

    public String getLBL_REP() {
        return Html.fromHtml(LBL_REP).toString();
    }

    public void setLBL_REP(String LBL_REP) {
        this.LBL_REP = LBL_REP;
    }

    public Boolean getGOOD() {
        return GOOD;
    }

    public void setGOOD(Boolean GOOD) {
        this.GOOD = GOOD;
    }

    public Integer getID_REP() {
        return ID_REP;
    }

    public void setID_REP(Integer ID_REP) {
        this.ID_REP = ID_REP;
    }
}
