package com.example.drunckedlock.Tools;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;

import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class CallAPI extends AsyncTask<String, String, Response> {
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override//async task
    protected Response doInBackground(String... params) {
        String urlString = params[0]; // URL to call
        String data = params[1]; //data to post

        Response response = null;
        try {
            //get the aswers of API
            response = (new OkHttpClient()).newCall(new Request.Builder().url(urlString).post(new MultipartBody.Builder().setType(MultipartBody.FORM).addFormDataPart("json", data).build()).build()).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;//return response
    }

    @Override//executed when get response
    protected void onPostExecute(Response response) {
        if (response == null) {
            Log.i("ERROR L37", "----------------------> La requete à rendu null !!");
        } else {
            String rep = null;
            try {
                rep = response.body().string();//try for catch possibly error
            } catch (Exception e) {
                Log.i("ERROR L43", "-------------------->" + e.getMessage());
            }
        }
    }
}
