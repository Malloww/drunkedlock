package com.example.drunckedlock.Tools;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.Html;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Quest {
    private ArrayList<Answer> ReponseList;
    private Integer ID_QUE;
    private String LBL_QUE;

    public Quest(ArrayList<Answer> reponseList, Integer ID_QUE, String LBL_QUE) {
        this.setReponseList(reponseList); //All answers for this quest
        this.setID_QUE(ID_QUE); // ID
        this.setLBL_QUE(LBL_QUE); // string of the Quest
    }

    //Convert OKHTTP return to Quest object
    public static ArrayList<Quest> parseJson(String rep) throws JSONException {

        JSONArray result = new JSONArray(rep); // Convert the OkHTTP return to JSON object
        ArrayList<Quest> Questions = new ArrayList<Quest>();

        for (Integer i = 0; i < result.length(); i++) {
            try {
                JSONObject ligne = new JSONObject(result.get(i).toString()); // One ligne of okhttp json answer's

                Integer id_quest = ligne.getInt("ID_QUE"); // initialisation
                String lbl_que = ligne.getString("LBL_QUE");
                ArrayList<Answer> answers = new ArrayList<Answer>();

                while (ligne.getInt("ID_QUE") == id_quest) {// Foreach Answer's of the same quest

                    answers.add(Answer.parseJson(ligne.toString()));//add the aswer to the array
                    i++;

                    try {
                        ligne = new JSONObject((result.get(i)).toString()); // try to recup the ligne "Answer" of the current Quest
                    } catch (Exception e) {
                        Log.i("ERROR 42", "-------------->Array out of range i = " + i);//Fix this later
                        break;
                    }
                }
                i--;

                Questions.add(new Quest(answers, id_quest, lbl_que));//add quest to array with  all answers

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return Questions;//return the object
    }

    //select all quest in local database
    public static ArrayList<Quest> selectAll(SQLiteDatabase db) {

        Cursor c = db.rawQuery("SELECT * FROM QUESTION", null);//the bdd request
        c.moveToFirst();//place the cursor on the top

        ArrayList<Quest> LQ = new ArrayList<Quest>();

        for (int i = 0; i < c.getCount(); i++) {//read all row of the request
            LQ.add(new Quest(Answer.selectManyLikeQuest(db, c.getInt(0)), c.getInt(0), c.getString(1)));
            c.move(1);//down the cursor
        }
        return LQ;
    }

    //insert one quest and his answer on local bdd and possibly in distant API
    public void insert(SQLiteDatabase db, Boolean init) {

        db.execSQL("INSERT INTO QUESTION (ID_QUE, LBL_QUE) VALUES (\"" + this.getID_QUE() + "\",\"" + this.getLBL_QUE() + "\")");//the bdd request

        for (int i = 0; i < this.getReponseList().size(); i++) {
            this.getReponseList().get(i).insert(db);//insert in local bdd
        }
        if (!init) {//pass that if you want to insert in distant API
            (new CallAPI()).execute("http://drunkedlock.000webhostapp.com/?Q=true", "{ \"Quest\":\"" + this.getLBL_QUE() + "\"," + "\"" + ((this.getReponseList().get(0).getGOOD()) ? "GoodRep" : "BadRep1") + "\":\"" + this.getReponseList().get(0).getLBL_REP() + "\"," + "\"" + ((this.getReponseList().get(1).getGOOD()) ? "GoodRep" : "BadRep2") + "\":\"" + this.getReponseList().get(1).getLBL_REP() + "\"," + "\"" + ((this.getReponseList().get(2).getGOOD()) ? "GoodRep" : "BadRep3") + "\":\"" + this.getReponseList().get(2).getLBL_REP() + "\"," + "\"" + ((this.getReponseList().get(3).getGOOD()) ? "GoodRep" : "BadRep4") + "\":\"" + this.getReponseList().get(3).getLBL_REP() + "\"" + "}");
        }
    }

    //select one quest on local bdd
    public Quest selectOne(SQLiteDatabase db) {
        Cursor c = db.rawQuery("SELECT * FROM QUESTION WHERE ID_QUE = " + this.ID_QUE, null);//the bdd request
        c.moveToFirst();
        this.setLBL_QUE(c.getString(1));//get the string of the quest
        this.setReponseList(Answer.selectManyLikeQuest(db, this.getID_QUE()));//get all answers for this quest
        return this;
    }

    //search all quest where string of quest like this
    public ArrayList<Quest> selectManyLikeThis(SQLiteDatabase db) {
        Cursor c = db.rawQuery("SELECT * FROM QUESTION WHERE LBL_QUE LIKE %" + this.getLBL_QUE() + "%", null);//the bdd request
        c.moveToFirst();
        ArrayList<Quest> LQ = null;

        for (int i = 0; i < c.getCount(); i++) {
            LQ.add(new Quest(Answer.selectManyLikeQuest(db, c.getInt(0)), c.getInt(0), c.getString(1)));//add to array all results
        }
        return LQ;
    }

    //update one quest in local bdd
    public void update(SQLiteDatabase db) {
        db.execSQL("UPDATE QUESTION SET LBL_QUE = " + this.getLBL_QUE() + " WHERE ID_QUE = " + this.getID_QUE());//the bdd request
        for (int i = 0; i < this.getReponseList().size(); i++) {
            this.getReponseList().get(i).update(db);//update all qeust of this quest
        }
    }

    //delete this quest in local bdd
    public void delete(SQLiteDatabase db) {
        db.execSQL("DELETE FROM QUESTION WHERE ID_QUE = " + this.getID_QUE());//the request bdd
    }

    public Integer getID_QUE() {
        return ID_QUE;
    }

    public void setID_QUE(Integer ID_QUE) {
        this.ID_QUE = ID_QUE;
    }

    public String getLBL_QUE() {
        return Html.fromHtml(LBL_QUE).toString();
    }

    public void setLBL_QUE(String LBL_QUE) {
        this.LBL_QUE = LBL_QUE;
    }

    public ArrayList<Answer> getReponseList() {
        return ReponseList;
    }

    public void setReponseList(ArrayList<Answer> reponseList) {
        ReponseList = reponseList;
    }
}
