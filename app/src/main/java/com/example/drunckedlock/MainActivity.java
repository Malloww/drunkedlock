package com.example.drunckedlock;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.drunckedlock.Lock.LockActivity;
import com.example.drunckedlock.Tools.NetworkTools;

public class MainActivity extends AppCompatActivity
{
    final private String sharedPrefFile = "com.example.drunckedlock";
    final private String PHONE_KEY = "phone";

    private String mPhoneNumber;
    private EditText editPhone;

    @Override public void onCreate(Bundle savedInstanceState) 
   {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NetworkTools NT = new NetworkTools();
        NT.setC(this.getApplicationContext());
        NT.execute();

       //pour rajouter une question
        /*
        ArrayList<Answer> listRep = new ArrayList<>();
        listRep.add(new Answer(41, 3, "Du lait", false));
        listRep.add(new Answer(42, 3, "De l'huile", false));
        listRep.add(new Answer(43, 3, "De l'eau", true));
        listRep.add(new Answer(44, 3, "Vodka !", false));
       (new Quest(listRep, 3, "Quue bois une vache quand elle a soife ?")).insert(new MySQLiteHelper(this.getApplicationContext()).getReadableDatabase(), false);
        */

       //On récupère la valeur du SharedPreferences
        SharedPreferences sharedPref = getSharedPreferences(sharedPrefFile, MODE_PRIVATE);
        mPhoneNumber = sharedPref.getString(PHONE_KEY, "0000");

        //On met la valeur dans le EditText
       editPhone = findViewById(R.id.editPhone);
        editPhone.setText(mPhoneNumber);

        editPhone.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.darker_gray), PorterDuff.Mode.SRC_ATOP);

        findViewById(R.id.ButtonLock).setOnClickListener(new View.OnClickListener() {
           public void onClick(View v) {
               //On récupère la valeur de EditText et on la rentre dans les SharedPreferences
               mPhoneNumber = editPhone.getText().toString();
               SharedPreferences.Editor editor = sharedPref.edit();
               editor.putString(PHONE_KEY, mPhoneNumber);
               editor.apply();
                if(!mPhoneNumber.isEmpty()) {
                    editPhone.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.darker_gray), PorterDuff.Mode.SRC_ATOP);
                    startActivity(new Intent(MainActivity.this, LockActivity.class));
                    finish();
                }
                else {
                    //SNACKBAR (ou Toast mais moins joli) A AFFICHER
                    //Toast.makeText(MainActivity.this, "Veuillez rentrer un numéro valide.", Toast.LENGTH_LONG).show();

                    Snackbar.make(findViewById(android.R.id.content),"Veuillez d'abord rentrer un numéro de téléphone valide pour continuer",Snackbar.LENGTH_LONG).show();

                    //Change la couleur du trait du EditText
                    editPhone.getBackground().mutate().setColorFilter(getResources().getColor(android.R.color.holo_red_light), PorterDuff.Mode.SRC_ATOP);
                }
           }
        });
   }
}
